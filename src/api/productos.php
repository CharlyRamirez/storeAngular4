<?php
// header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
function conecta_db(){
    // $servidor = "192.168.1.250";
    $servidor = "177.227.109.25";
    $usuario = "web";
    $clave = "webfmolvera17";
    $db = "datosa";

    $conn = mysqli_connect($servidor, $usuario, $clave, $db);

    if(mysqli_connect_errno()){
        echo mysqli_connect_error();
        exit(0);
    }

    return $conn;
}

$opcion = $_GET["opcion"];

if(isset($_GET["id"])){
    $id = $_GET["id"];
}

if(isset($_GET["perid"])){
    $perid = $_GET["perid"];
}

$conn = conecta_db();
switch($opcion){
    case 1:
        $sql = "SELECT
                    INV.CLVPROV AS CODIGO,
                    INV.CLAVE AS CLAVE,
                    INV.DESCRIPCIO AS DESCRIPCION,
                    INV.INVDESCUENTO AS DESCUENTO,
                    ROUND((RINV11.PRECIO + (((((1 + (IF(INV.IMPUESTO2 = 'S', RINV11.PIMPUESTO / 100, 0) + INV.INVIMPUESTOL / 100)) * (1 + INV.INVIEPS / 100)) * 100 - 100) / 100) * RINV11.PRECIO)) - ((INV.INVDESCUENTO / 100) * (RINV11.PRECIO + (((((1 + (IF(INV.IMPUESTO2 = 'S', RINV11.PIMPUESTO / 100, 0) + INV.INVIMPUESTOL / 100)) * (1 + INV.INVIEPS / 100)) * 100 - 100) / 100) * RINV11.PRECIO)))) AS PRECIO,
                    INV.DIBUJO AS DIBUJO,
                    INV.ARTICULOID AS ID
                    FROM INV
                        LEFT JOIN CALPREUNI AS RINV1
                            ON INV.ARTICULOID = RINV1.ARTICULOID
                                AND INV.UNIVENID = RINV1.UNIDADID
                                AND RINV1.NPRECIO = '3'
                        LEFT JOIN PRECIOFINAL AS RINV11
                            ON RINV1.ARTICULOID = RINV11.ARTICULOID
                                AND RINV1.UNIDADID = RINV11.UNIDADID
                                AND RINV11.NPRECIO = '3'
                    WHERE (INV.USADO <> 'D')
                        AND (INSTR(INV.DESCRIPCIO, 'AJUSTE POR') = 0)
                        AND INV.INVDESCUENTO > 0
                        AND ((SELECT IFNULL(SUM(AA.EXISTENCIA), 0) FROM ALM AA
                            WHERE AA.ARTICULOID = INV.ARTICULOID) > 0)
                    ORDER BY CODIGO ASC";
    break;
    case 2:
        $fechaIni = date('Y-m-01');
        $fechaFin = date('Y-m-d');
        $int = rand(500, 505);
        $sql = "SELECT
                    RDES1.ARTICULOID AS C36601,
                    IFNULL(SUM(DES.DESCANTIDAD * DES.DESEQUIVALE), 0) AS C36631,
                    RDES1.DESCRIPCIO AS DESCRIPCION,
                    RDES1.CLVPROV AS CODIGO,
                    ROUND(RDES11.PRECIO + (RDES11.PRECIO * (RDES11.PIMPUESTO / 100))) AS PRECIO,
                    RDES1.CLAVE AS CLAVE
                    FROM DES
                        LEFT JOIN INV AS RDES1
                            ON DES.DESARTID = RDES1.ARTICULOID
                        LEFT JOIN PRECIOFINAL AS RDES11
                            ON RDES1.ARTICULOID = RDES11.ARTICULOID
                                AND RDES1.UNIVENID = RDES11.UNIDADID
                                AND RDES11.NPRECIO = '3'
                    WHERE (DES.DESTIPO = 'E')
                        AND ((DES.DESFECHA BETWEEN '$fechaIni' AND '$fechaFin'))
                        AND ROUND(RDES11.PRECIO + (RDES11.PRECIO * (RDES11.PIMPUESTO / 100))) > 500
                    GROUP BY RDES1.CLAVE
                    ORDER BY C36631 DESC
                    LIMIT 0,4";
    break;
    case 3:
        $sql = "SELECT
                    RDES1.CLAVE, RDES1.CLVPROV, RDES1.DESCRIPCIO,
                    ROUND((RDES11.PRECIO + (((((1 + (IF(RDES1.IMPUESTO2 = 'S', RDES11.PIMPUESTO / 100, 0) + RDES1.INVIMPUESTOL / 100)) * (1 + RDES1.INVIEPS / 100)) * 100 - 100) / 100) * RDES11.PRECIO)) - ((RDES1.INVDESCUENTO / 100) * (RDES11.PRECIO + (((((1 + (IF(RDES1.IMPUESTO2 = 'S', RDES11.PIMPUESTO / 100, 0) + RDES1.INVIMPUESTOL / 100)) * (1 + RDES1.INVIEPS / 100)) * 100 - 100) / 100) * RDES11.PRECIO)))) AS PRECIO
                    FROM INV AS RDES1
                        LEFT JOIN PRECIOFINAL AS RDES11
                            ON RDES1.ARTICULOID = RDES11.ARTICULOID
                            AND RDES1.UNIVENID = RDES11.UNIDADID
                            AND RDES11.NPRECIO = '3'
                    WHERE RDES1.CLVPROV = $id";
    break;
    case 4:
        $sql = "SELECT PERID FROM PER WHERE CATEGORIA = '$usuario' AND PERID = $password";
    break;
    case 5:
        $sql = "SELECT * FROM PER WHERE PERID = $perid";
    break;
    case 6:
        if(isset($_GET["buscar"])){
            $buscar = $_GET["buscar"];
        }
        $sql = "SELECT
                    RDES1.CLAVE, RDES1.CLVPROV, RDES1.DESCRIPCIO,
                    ROUND((RDES11.PRECIO + (((((1 + (IF(RDES1.IMPUESTO2 = 'S', RDES11.PIMPUESTO / 100, 0) + RDES1.INVIMPUESTOL / 100)) * (1 + RDES1.INVIEPS / 100)) * 100 - 100) / 100) * RDES11.PRECIO)) - ((RDES1.INVDESCUENTO / 100) * (RDES11.PRECIO + (((((1 + (IF(RDES1.IMPUESTO2 = 'S', RDES11.PIMPUESTO / 100, 0) + RDES1.INVIMPUESTOL / 100)) * (1 + RDES1.INVIEPS / 100)) * 100 - 100) / 100) * RDES11.PRECIO)))) AS PRECIO
                    FROM INV AS RDES1
                        LEFT JOIN PRECIOFINAL AS RDES11
                            ON RDES1.ARTICULOID = RDES11.ARTICULOID
                            AND RDES1.UNIVENID = RDES11.UNIDADID
                            AND RDES11.NPRECIO = '3'
                    WHERE
                        (
                            (RDES1.CLAVE LIKE '$buscar%')
                            OR (RDES1.CLVPROV LIKE '$buscar%')
                            OR (RDES1.DESCRIPCIO LIKE '%$buscar%')
                        )";
    break;
}
$rs = mysqli_query($conn, $sql);
$array = array();
if($rs){
    while($fila = mysqli_fetch_assoc($rs)){
        $array[] = array_map('utf8_encode', $fila);
    }
    $res = json_encode($array, JSON_NUMERIC_CHECK);
}else{
    $res = null;
    echo mysqli_error($conn);
}
mysqli_close($conn);
echo $res;
?>