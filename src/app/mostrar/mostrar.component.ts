import { Component, OnInit, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConexionService } from '../services/conexion.service';
// import { BuscarDirective } from '../directives/buscar.directive';
import "rxjs/add/operator/map";

@Component({
  selector: 'app-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.css']
})
export class MostrarComponent implements OnInit {
  buscando = this.route.snapshot.params["buscando"];

  filterargs;
  listado;
  items;

  constructor(private conexion: ConexionService,
              private route: ActivatedRoute,
              private router: Router){
              }

  ngOnInit() {
    this.conexion.buscarProd(this.buscando)
    .map((response) => response.json())
    .subscribe((data) => {
      this.listado = data;
    })
  }
}
