import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { DetalleComponent } from './detalle/detalle.component';
import { StoreComponent } from './store/store.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { BuscarComponent } from './buscar/buscar.component';
import { MostrarComponent } from './mostrar/mostrar.component';

import { ConexionService } from './services/conexion.service';
import { AutorizacionService } from './services/autorizacion.service';
import { SearchDiariosService } from './services/search-diarios.service';
import { NotFoundComponent } from './not-found/not-found.component';

const appRoutes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'index', component: IndexComponent},
  {path: 'tienda', component: StoreComponent},
  {path: 'detalle/:item.CODIGO', component: DetalleComponent},
  {path: 'mostrar/:buscando', component: MostrarComponent},
  {path: 'buscar/:params', component: BuscarComponent},
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminComponent},
  {path: '404', component: NotFoundComponent},
  {path: '**', redirectTo: "/404"}
];

export const firebaseConfig = {
  apiKey: "AIzaSyCJun0yq9DLWAP9oj0JTg76RD9mCOeBL6k",
  authDomain: "fmostore-25ecd.firebaseapp.com",
  databaseURL: "https://fmostore-25ecd.firebaseio.com",
  projectId: "fmostore-25ecd",
  storageBucket: "fmostore-25ecd.appspot.com",
  messagingSenderId: "683169867926"
};

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    DetalleComponent,
    StoreComponent,
    AdminComponent,
    LoginComponent,
    BuscarComponent,
    MostrarComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [ConexionService, AutorizacionService, SearchDiariosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
