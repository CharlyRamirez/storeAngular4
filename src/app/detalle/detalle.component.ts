import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConexionService } from '../services/conexion.service';
import "rxjs/add/operator/map";

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {
  codigo = this.route.snapshot.params["item.CODIGO"];
  filterargs;
  listado;
  items;

  constructor(private route: ActivatedRoute, private conexion: ConexionService){}

  ngOnInit() {
    this.conexion.detallar(this.codigo)
    .map((response) => response.json())
    .subscribe((data) => {
      this.listado = data;
    })
  }

}
