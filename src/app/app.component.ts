import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConexionService } from './services/conexion.service';
import { AutorizacionService } from './services/autorizacion.service';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  buscando;
  // listado:Array<any>;
  loggedIn = false;
  loggedUser:any = null;
  loggedImg:any = null;
  loggedAdmin:any = false;
  imgOk = false;
  constructor(private conexion: ConexionService, private router: Router, private route: ActivatedRoute, private autorizacionService: AutorizacionService){
    this.autorizacionService.isLogged()
    .subscribe((result) => {
      if(result && result.uid){
        this.loggedIn = true;
        setTimeout(() => {
          if(this.autorizacionService.getUser().currentUser.displayName > ''){
            this.loggedUser = this.autorizacionService.getUser().currentUser.displayName;
            this.loggedAdmin = false;
          }else{
            this.loggedUser = this.autorizacionService.getUser().currentUser.email;
            this.loggedAdmin = true;
          }
          if(this.autorizacionService.getUser().currentUser.photoURL > ''){
            this.imgOk = true;
            this.loggedImg = this.autorizacionService.getUser().currentUser.photoURL;
          }else{
            this.imgOk = false;
          }
        }, 500);
      }else{
        this.loggedIn = false;
      }
    }, (error) => {
      this.loggedIn = false;
    })
  }

  logout(){
    this.autorizacionService.logout();
    this.router.navigate(['/'])
  }

  buscarProd(){
    this.router.navigate(['mostrar/'+this.buscando]);
  }
}
