import { Component, OnInit } from '@angular/core';
import { AutorizacionService } from '../services/autorizacion.service';
import { Router } from '@angular/router';

import { SearchDiariosService } from '../services/search-diarios.service';
import "rxjs/add/operator/map";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  loggedIn = false;
  loggedUser:any = null;
  loggedImg:any = null;
  imgOk = false;

  divEscritorio = true;
  divRGeneral = false;
  divRServicio = false;
  divBackOrder = false;
  divCtaEsp = false;
  divDiarios = false;
  divLineaTiempo = false;

  selectDiario = null;
  mostrarNada = true;
  mostrarDiarioVta = false;
  mostrarDiarioComp = false;
  mostrarUtilidades = false;
  mostrarNotCred = false;

  fechas = false;
  vendedores = false;
  proveedores = false;
  botonEnviar = false;
  diarioVta = false;
  diarioComp = false;
  divDocsComPro = false;

  fecIni;
  fecFin;
  selecVen;
  selecProv;
  perId;
  clienteid;
  listaVen;
  listaProv;
  listDVta;
  sumListDVta;
  listDComp;
  sumlistDComp;
  dComPro;
  nomProv;

  constructor(private autorizacionService: AutorizacionService, private router: Router, private searchDiarios: SearchDiariosService){
    this.autorizacionService.isLogged()
    .subscribe((result) => {
      if(result && result.uid){
        this.loggedIn = true;
        setTimeout(() => {
          if(this.autorizacionService.getUser().currentUser.displayName > ''){
            this.loggedUser = this.autorizacionService.getUser().currentUser.displayName;
          }else{
            this.loggedUser = this.autorizacionService.getUser().currentUser.email;
          }
          if(this.autorizacionService.getUser().currentUser.photoURL > ''){
            this.imgOk = true;
            this.loggedImg = this.autorizacionService.getUser().currentUser.photoURL;
          }else{
            this.imgOk = false;
          }
        }, 500);
      }else{
        this.loggedIn = false;
        this.router.navigate(['/'])
      }
    }, (error) => {
      this.loggedIn = false;
      this.router.navigate(['/'])
    })
  }

  logout(){
    this.autorizacionService.logout();
    this.router.navigate(['/'])
  }

  escritorio(){
    this.divRGeneral = false;
    this.divRServicio = false;
    this.divBackOrder = false;
    this.divCtaEsp = false;
    this.divDiarios = false;
    this.divLineaTiempo = false;
    this.divEscritorio = true;
  }
  rGen(){
    this.divEscritorio = false;
    this.divRServicio = false;
    this.divBackOrder = false;
    this.divCtaEsp = false;
    this.divDiarios = false;
    this.divLineaTiempo = false;
    this.divRGeneral = true;
  }
  rSer(){
    this.divEscritorio = false;
    this.divRGeneral = false;
    this.divBackOrder = false;
    this.divCtaEsp = false;
    this.divDiarios = false;
    this.divLineaTiempo = false;
    this.divRServicio = true;
  }
  diarios(){
    this.divEscritorio = false;
    this.divRGeneral = false;
    this.divRServicio = false;
    this.divBackOrder = false;
    this.divCtaEsp = false;
    this.divLineaTiempo = false;
    this.divDiarios = true;
  }
  linTiem(){
    this.divEscritorio = false;
    this.divRGeneral = false;
    this.divRServicio = false;
    this.divBackOrder = false;
    this.divCtaEsp = false;
    this.divDiarios = false;
    this.divLineaTiempo = true;
  }

  ngOnInit() {
    this.searchDiarios.listVen()
    .map((response) => response.json())
    .subscribe((data) => {
      this.listaVen = data;
    });

    this.searchDiarios.listaProv()
    .map((response) => response.json())
    .subscribe((data) => {
      this.listaProv = data;
    });
  }

  reporteDiario(){
    switch(this.selectDiario){
      case '0':
        this.mostrarNada = true;
        this.mostrarDiarioVta = false;
        this.mostrarDiarioComp = false;
        this.mostrarUtilidades = false;
        this.mostrarNotCred = false;

        this.fechas = false;
        this.vendedores = false;
        this.proveedores = false;
        this.botonEnviar = false;
        break;
      case '1':
        this.mostrarNada = false;
        this.mostrarDiarioVta = true;
        this.mostrarDiarioComp = false;
        this.mostrarUtilidades = false;
        this.mostrarNotCred = false;
        this.botonEnviar = true;

        this.fechas = true;
        this.vendedores = true;
        this.proveedores = false;

        break;
      case '2':
        this.mostrarNada = false;
        this.mostrarDiarioVta = false;
        this.mostrarDiarioComp = true;
        this.mostrarUtilidades = false;
        this.mostrarNotCred = false;
        this.botonEnviar = true;

        this.fechas = true;
        this.vendedores = false;
        this.proveedores = true;
        break;
      case '3':
        this.mostrarNada = false;
        this.mostrarDiarioVta = false;
        this.mostrarDiarioComp = false;
        this.mostrarUtilidades = true;
        this.mostrarNotCred = false;
        this.botonEnviar = true;

        this.fechas = true;
        this.vendedores = false;
        this.proveedores = false;
        break;
      case '4':
        this.mostrarNada = false;
        this.mostrarDiarioVta = false;
        this.mostrarDiarioComp = false;
        this.mostrarUtilidades = false;
        this.mostrarNotCred = true;
        this.botonEnviar = true;

        this.fechas = true;
        this.vendedores = false;
        this.proveedores = false;
        break;
    }
  }

  buscarDVta(){
    if(this.selecVen > 1){
      this.perId = this.selecVen;
    }else{
      this.perId = 0;
    }

    let fechaActual = new Date();
    let dia = fechaActual.getDate().toString();
    let mes = (fechaActual.getMonth() + 1).toString();
    let anio = fechaActual.getFullYear().toString();

    if(this.fecIni == null){
      this.fecIni = anio + "-" + mes + "-" + dia;
    }

    if(this.fecFin == null){
      this.fecFin = anio + "-" + mes + "-" + dia;
    }

    this.searchDiarios.diarioVenta(this.perId, this.fecIni, this.fecFin)
        .map((response) => response.json())
        .subscribe((data) => {
          this.listDVta = data;
        })
    
    this.searchDiarios.sumDiarioVenta(this.perId, this.fecIni, this.fecFin)
        .map((response) => response.json())
        .subscribe((data) => {
          this.sumListDVta = data;
        })
    this.diarioVta = true;
  }

  buscarDComp(){
    if(this.selecProv > 1){
      this.clienteid = this.selecProv;
    }else{
      this.clienteid = 0;
    }

    let fechaActual = new Date();
    let dia = fechaActual.getDate().toString();
    let mes = (fechaActual.getMonth() + 1).toString();
    let anio = fechaActual.getFullYear().toString();

    if(this.fecIni == null){
      this.fecIni = anio + "-" + mes + "-" + dia;
    }

    if(this.fecFin == null){
      this.fecFin = anio + "-" + mes + "-" + dia;
    }

    this.searchDiarios.diarioCompra(this.clienteid, this.fecIni, this.fecFin)
        .map((response) => response.json())
        .subscribe((data) =>{
          this.listDComp = data;
        })

    this.diarioComp = true;
  }

  mostrarDocsComProv(cid, nombre){
    this.searchDiarios.docsCompProv(cid, this.fecIni, this.fecFin)
        .map((response) => response.json())
        .subscribe((data) => {
          this.dComPro = data;
        })
    
    this.nomProv = nombre;
    this.divDocsComPro = true;
  }

  cerrarModalDocComPro(){
    this.divDocsComPro = false;
  }
}
