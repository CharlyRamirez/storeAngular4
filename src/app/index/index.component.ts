import { Component, OnInit } from '@angular/core';
import { ConexionService } from '../services/conexion.service';
import { ActivatedRoute } from '@angular/router';
import "rxjs/add/operator/map";
import { element } from 'protractor';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit{
  public mostrarT = true;
  public mostrarP = false;
  public mostrarV = false;
  public mostrarF = false;
  public mostrarFi = false;
  public mostrarK = false;
  public mostrarH = false;
  marca:any = null;
  filterargs;
  listado;
  items;

  constructor(private conexion: ConexionService, private route: ActivatedRoute){
    // console.log(this.route.snapshot.queryParams["barraSearch"]);
  }

  ngOnInit() {
    this.conexion.vendido()
    .map((response) => response.json())
    .subscribe((data) => {
      this.listado = data;
    })
  }

  muestra(event){
    this.marca = event.path[1].id;
    switch(this.marca){
      case 'truper':
        this.mostrarT = true;
        this.mostrarP = false;
        this.mostrarV = false;
        this.mostrarF = false;
        this.mostrarFi = false;
        this.mostrarK = false;
        this.mostrarH = false;
      break;
      case 'pretul':
      this.mostrarT = false;
        this.mostrarP = true;
        this.mostrarV = false;
        this.mostrarF = false;
        this.mostrarFi = false;
        this.mostrarK = false;
        this.mostrarH = false;
      break;
      case 'volteck':
        this.mostrarT = false;
        this.mostrarP = false;
        this.mostrarV = true;
        this.mostrarF = false;
        this.mostrarFi = false;
        this.mostrarK = false;
        this.mostrarH = false;
      break;
      case 'foset':
        this.mostrarT = false;
        this.mostrarP = false;
        this.mostrarV = false;
        this.mostrarF = true;
        this.mostrarFi = false;
        this.mostrarK = false;
        this.mostrarH = false;
      break;
      case 'fiero':
        this.mostrarT = false;
        this.mostrarP = false;
        this.mostrarV = false;
        this.mostrarF = false;
        this.mostrarFi = true;
        this.mostrarK = false;
        this.mostrarH = false;
      break;
      case 'klintek':
        this.mostrarT = false;
        this.mostrarP = false;
        this.mostrarV = false;
        this.mostrarF = false;
        this.mostrarFi = false;
        this.mostrarK = true;
        this.mostrarH = false;
      break;
      case 'hermex':
        this.mostrarT = false;
        this.mostrarP = false;
        this.mostrarV = false;
        this.mostrarF = false;
        this.mostrarFi = false;
        this.mostrarK = false;
        this.mostrarH = true;
      break;
    }
  }

}
