import { Component } from '@angular/core';
// import { ActivatedRoute, Router } from '@angular/router';
// import { ConexionService } from '../services/conexion.service';
import { AutorizacionService } from '../services/autorizacion.service';

import "rxjs/add/operator/map";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{
  // sesion = this.route.snapshot.queryParams["sesion"];
  // filterargs;
  // listado;
  // items;
  // public usuario:string = '';
  // password:number = null;

  loginParams:any = {};

  constructor(private autorizacionService: AutorizacionService){}

  login(){
    this.autorizacionService.login(this.loginParams.email, this.loginParams.password);
  }

  facebookLogin(){
    this.autorizacionService.facebookLogin();
  }

  // iniciar(){
  //   this.conexion.sesionAdmin(this.usuario, this.password)
  //   .map((response) => response.json())
  //   .subscribe((data) => {
  //     this.listado = data;
  //     console.log(this.listado[0]["PERID"]);
  //     this.router.navigate(['admin/'+this.listado[0]["PERID"]]);
  //   })
  // }
}
