import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class SearchDiariosService {

  constructor(private http: Http){}

  diarioVenta(id: number, fecIni: string, fecFin: string){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=1&id=' + id + '&fecIni=' + fecIni + '&fecFin=' + fecFin);
  }

  diarioCompra(clienteid: number, fecIni: string, fecFin: string){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=2&clienteid=' + clienteid + '&fecIni=' + fecIni + '&fecFin=' + fecFin);
  }

  utilidades(){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=3');
  }

  notasCredito(){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=4');
  }

  listVen(){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=6');
  }

  sumDiarioVenta(id: number, fecIni: string, fecFin: string){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=7&id=' + id + '&fecIni=' + fecIni + '&fecFin=' + fecFin);
  }

  listaProv(){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=8');
  }

  docsCompProv(clienteid: number, fecIni: string, fecFin: string){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/diarios.php?opcion=9&clienteid=' + clienteid + '&fecIni=' + fecIni + '&fecFin=' + fecFin);
  }
}
