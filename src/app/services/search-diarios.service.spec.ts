import { TestBed, inject } from '@angular/core/testing';

import { SearchDiariosService } from './search-diarios.service';

describe('SearchDiariosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchDiariosService]
    });
  });

  it('should be created', inject([SearchDiariosService], (service: SearchDiariosService) => {
    expect(service).toBeTruthy();
  }));
});
