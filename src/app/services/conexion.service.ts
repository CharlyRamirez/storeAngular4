import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ConexionService {
  session:any = null;
  usuario:any = null;

  constructor(private http: Http){}
  
  promocion(){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/productos.php?opcion=1');
  }

  vendido(){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/productos.php?opcion=2');
  }

  detallar(id: number){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/productos.php?opcion=3&id=' + id);
  }

  buscarProd(buscando:string){
    return this.http.get('http://fmo.charlyramirez.com.mx/store/src/api/productos.php?opcion=6&buscar=' + buscando);
  }
}
