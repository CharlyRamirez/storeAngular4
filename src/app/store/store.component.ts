import { Component, OnInit } from '@angular/core';
import { ConexionService } from '../services/conexion.service';
import "rxjs/add/operator/map";

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  filterargs;
  listado;
  items;

  constructor(private conexion: ConexionService) { }

  ngOnInit() {
    this.conexion.promocion()
    .map((response) => response.json())
    .subscribe((data) => {
      this.listado = data;
    })
  }
}
